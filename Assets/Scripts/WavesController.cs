﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WavesController : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] Transform spawnpoint;
    [SerializeField] float timebetweenWaves = 20f;
    [SerializeField] TextMeshProUGUI wavesTimerText;

    float countdown = 2f;
    int waveIndex = 0;

    SpawnSystem spawnSystem;
    GameManager gameManager;

    public Transform Spawnpoint { get => spawnpoint; set => spawnpoint = value; }

    private void Start()
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
        gameManager = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timebetweenWaves;
        }
        countdown -= Time.deltaTime;
        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);//значение таймера не может быть отрицательным

        wavesTimerText.text = "Next wave: " + string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave()
    {
        waveIndex++;
        gameManager.Rounds++;
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.5f);
        }
    }

    void SpawnEnemy()
    {
        GameObject enemy = spawnSystem.CheckPool("Enemy");

        if(enemy != null)
        {
            enemy.transform.position = spawnpoint.position;
            enemy.gameObject.SetActive(true);
        }
        else
        {
            enemy = Instantiate(enemyPrefab, spawnpoint.position, spawnpoint.rotation);
        }

    }
}
