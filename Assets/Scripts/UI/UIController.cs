﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI moneyCounterText;
    [SerializeField] TextMeshProUGUI livesCounterText;

    public void UpdateMoneyCounter(int money)
    {
        moneyCounterText.text = "$" + money.ToString();
    }
    public void UpdateLivesCounter(int lives)
    {
        livesCounterText.text = lives.ToString() + " Lives";
    }
}
