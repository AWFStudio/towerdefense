﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopUI : MonoBehaviour
{
    [SerializeField] TowerBlueprint turretTower;
    [SerializeField] TowerBlueprint cannonTower;
    [SerializeField] TowerBlueprint mageTower;

    BuildManager buildManager;
    private void Start()
    {
        buildManager = BuildManager.instance;
    }
    public void SelectTurretTower()
    {
        buildManager.SelectTowerToBuild(turretTower);
    }
    public void SelectCannonTower()
    {
        buildManager.SelectTowerToBuild(cannonTower);
    }
    public void SelectMageTower()
    {
        buildManager.SelectTowerToBuild(mageTower);
    }
}
