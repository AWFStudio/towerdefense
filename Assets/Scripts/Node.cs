﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    [SerializeField] Color notEnoughMoneyColor;
    [SerializeField] Color hoverColor;
    [SerializeField] Vector3 positionOffset;
    [SerializeField] GameObject buildEffect;

    [Header("Optional")]
    [SerializeField] GameObject tower;

    Color startColor;
    Renderer rend;
    BuildManager buildManager;

    public GameObject Tower { get => tower; set => tower = value; }

    private void Start()
    {
        buildManager = BuildManager.instance;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
    }

    private void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())//когда поверх этого элемента отображается UI элемент и курсор наведен на нее, не подсвечиваем область
        {
            return;
        }

        if (!buildManager.CanBuild)//если нельзя строить башню, не подсвечиваем области
        {
            return;
        }

        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColor;
        }
        else
        {
            rend.material.color = notEnoughMoneyColor;
        }
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (!buildManager.CanBuild)
        {
            return;
        }
        if (tower != null)
        {
            Debug.Log("Can`t build here!");
            return;
        }

        buildManager.BuildTowerOn(this);
    }

    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    public void ShowBuildEffect()
    {
        buildEffect.SetActive(true);
    }


}
