﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] int startMoney = 400;
    [SerializeField] int startLives = 20;
    [SerializeField] GameObject gameOverPanel;

    bool gameOver;
    int money;
    int lives;
    int rounds;

    UIController ui;

    public int Money { get => money;}
    public int Rounds { get => rounds; set => rounds = value; }

    void Start()
    {
        ui = FindObjectOfType<UIController>();

        money = startMoney;
        ui.UpdateMoneyCounter(money);

        lives = startLives;
        ui.UpdateLivesCounter(lives);

        rounds = 0;
    }

    private void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            Endgame();
        }
    }

    public void ReduceMoneyAmount(int amount)
    {
        money -= amount;
        ui.UpdateMoneyCounter(money);
    }

    public void EnlargeMoneyAmount(int amount)
    {
        money += amount;
        ui.UpdateMoneyCounter(money);
    }
    public void ReduceLivesAmount()
    {
        if (gameOver)
        {
            return;
        }
        lives--;
        if (lives > 0)
        {
            ui.UpdateLivesCounter(lives);
        }
        else
        {
            Endgame();
        }
    }

    private void Endgame()
    {
        gameOver = true;
        gameOverPanel.SetActive(true);
    }
}
