﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class Effect : MonoBehaviour
{
    SpawnSystem spawnSystem;
    ParticleSystem ps;

    enum EffectType
    {
        hit,
        explosion,
        light
    }

    [SerializeField] EffectType type;

    private void Awake()
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
        ps = GetComponent<ParticleSystem>();
    }

    void OnEnable()
    {
        StartCoroutine(CheckIfAlive());
    }

    IEnumerator CheckIfAlive()
    {
        while (true && ps != null)
        {
            yield return new WaitForSeconds(0.5f);
            if (!ps.IsAlive(true))
            {
                if (type != EffectType.light)
                {
                    spawnSystem.PutToThePool(gameObject, type.ToString());
                }
                else
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
