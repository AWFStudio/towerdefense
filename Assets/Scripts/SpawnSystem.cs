﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem : MonoBehaviour
{
    [SerializeField] List<GameObject> enemiesPool = new List<GameObject>();
    [SerializeField] List<GameObject> bulletsPool = new List<GameObject>();
    [SerializeField] List<GameObject> cannonballsPool = new List<GameObject>();
    [SerializeField] List<GameObject> hitEffectsPool = new List<GameObject>();
    [SerializeField] List<GameObject> explosionEffectsPool = new List<GameObject>();

    public GameObject CheckPool(string type)
    {
        List<GameObject> pool = new List<GameObject>();
        GameObject obj;
        switch (type)
        {
            case "Enemy":
                pool = enemiesPool;
                break;
            case "Turret Tower":
                pool = bulletsPool;
                break;
            case "Cannon Tower":
                pool = cannonballsPool;
                break;
            case "Hit":
                pool = hitEffectsPool;
                break;
            case "Explosion":
                pool = explosionEffectsPool;
                break;
        }

        if (pool.Count > 0)
        {
            obj = pool[0];
            pool.Remove(obj);
            return obj;
        }
        else
        {
            return null;
        }
    }

    public void PutToThePool(GameObject obj, string type)
    {
        List<GameObject> pool = new List<GameObject>();

        switch (type)
        {
            case "Enemy":
                pool = enemiesPool;
                break;
            case "Bullet":
                pool = bulletsPool;
                break;
            case "Cannon Ball":
                pool = cannonballsPool;
                break;
            case "hit":
                pool = hitEffectsPool;
                break;
            case "explosion":
                pool = explosionEffectsPool;
                break;
        }

        pool.Add(obj);
        obj.SetActive(false);
    }
}

