﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower : MonoBehaviour
{
    [SerializeField] protected string towerName;
    [SerializeField] protected Transform firePoint;
    [SerializeField] int health;
    [SerializeField] float range;

    protected Transform target;
    protected Enemy enemy;
    protected List<GameObject> enemiesInRange = new List<GameObject>();

    public List<GameObject> EnemiesInRange { get => enemiesInRange; set => enemiesInRange = value; }

    public virtual void Update()
    {
        if (target == null)
        {
            if (enemiesInRange.Count > 0)
            {
                target = enemiesInRange[0].transform;
                enemy = target.GetComponent<Enemy>();
            }
        }
        else
        {
            if (!enemiesInRange.Contains(target.gameObject) || Vector3.Distance(target.transform.position, transform.position) > range)
            {
                target = null;
                return;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Enemy>() != null)
        {
            enemiesInRange.Add(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Enemy>() != null)
        {
            enemiesInRange.Remove(other.gameObject);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
    }
}

