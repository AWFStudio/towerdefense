﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingTower : Tower
{
    [SerializeField] float fireRate;
    [SerializeField] Transform partToRotate;
    [SerializeField] GameObject projectilePref;
    [SerializeField] float turnSpeed;
    float FireCountdown = 0f;
    SpawnSystem spawnSystem;

    private void Start()
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
    }

    public override void Update()
    {
        base.Update();

        if (target == null)
        {
            if (enemiesInRange.Count > 0)
            {
                target = enemiesInRange[0].transform;
            }
        }
        else
        {
            Vector3 dir = target.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //плавный поворот
            partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
            if (FireCountdown <= 0f)
            {
                {
                    Shoot();
                }
                FireCountdown = 1f / fireRate;
            }
            FireCountdown -= Time.deltaTime;
        }
    }

    private void Shoot()
    {
        GameObject projectile = spawnSystem.CheckPool(towerName);

        if (projectile != null)
        {
            projectile.transform.position = firePoint.position;
            projectile.SetActive(true);
        }
        else
        {
            projectile = Instantiate(projectilePref, firePoint.position, firePoint.rotation);
        }

        projectile.GetComponent<Projectile>().Seek(target, this);
    }

}
