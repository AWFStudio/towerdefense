﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageTower : Tower
{
    [SerializeField] LineRenderer lineRenderer;
    [SerializeField] float damageOverTime = 30f;
    [SerializeField] float slowdownPercentage = 0.5f;

    public override void Update()
    {
        base.Update();

        if (target == null && lineRenderer.enabled)
        {
            lineRenderer.SetPosition(1, firePoint.position);
            lineRenderer.enabled = false;
        }
        else
        {
            Attack();
        }
    }

    void Attack()
    {
        if (target != null)
        {
            enemy.TakeDamage(this, damageOverTime * Time.deltaTime);
            enemy.Slowdown(slowdownPercentage);

            if (!lineRenderer.enabled)
            {
                lineRenderer.enabled = true;
            }
            lineRenderer.SetPosition(0, firePoint.position);
            lineRenderer.SetPosition(1, target.position);
        }
    }
}

