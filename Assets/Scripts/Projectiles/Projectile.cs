﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    [SerializeField] string projectileName;
    [SerializeField] string effectName;
    [SerializeField] GameObject effectPref;
    [SerializeField] protected int damage;
    [SerializeField] float speed;

    SpawnSystem spawnSystem;
    protected Transform target;
    protected Tower turret;
    protected Enemy enemy;

    public void Seek(Transform target, Tower turret)
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
        this.turret = turret;
        this.target = target;
        enemy = target.GetComponent<Enemy>();
    }

    void Update()
    {
        if (target == null || enemy.CurrentHealth <= 0)
        {
            spawnSystem.PutToThePool(gameObject, projectileName);
        }
        else
        {
            Vector3 dir = target.position - transform.position;
            float distanceThisFrame = speed * Time.deltaTime;

            if (dir.magnitude <= distanceThisFrame)
            {
                HitTarget();
                return;
            }

            transform.Translate(dir.normalized * distanceThisFrame, Space.World);
            transform.LookAt(target);
        }
    }

    public virtual void HitTarget()
    {
        GameObject effect = spawnSystem.CheckPool(effectName);
        if (effect != null)
        {
            effect.transform.position = transform.position;
            effect.SetActive(true);
        }
        else
        {
            Instantiate(effectPref, transform.position, transform.rotation);
        }
        spawnSystem.PutToThePool(gameObject, projectileName);
    }
}
