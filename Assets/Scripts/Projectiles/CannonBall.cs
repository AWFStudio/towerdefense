﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : Projectile
{
    [SerializeField] float explosionRadius;
    [SerializeField] LayerMask layerMask;

    public override void HitTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, layerMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].GetComponent<Enemy>().TakeDamage(turret, damage);
        }
        base.HitTarget();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
