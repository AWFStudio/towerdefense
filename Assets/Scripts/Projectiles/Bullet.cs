﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile
{
    public override void HitTarget()
    {
        target.gameObject.GetComponent<Enemy>().TakeDamage(turret, damage);
        base.HitTarget();
    }

}
