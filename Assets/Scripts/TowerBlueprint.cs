﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]//чтобы видеть поля этого класса в инспекторе
public class TowerBlueprint
{
    public GameObject towerPref;
    public int towerCost;
}
