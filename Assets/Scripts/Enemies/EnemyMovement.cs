﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] Enemy enemy;
    int wavepointIndex;
    SpawnSystem spawnSystem;
    GameManager gameManager;
    Transform targetPoint;
    Transform myTransform;

    private void Start()
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
        gameManager = FindObjectOfType<GameManager>();
        myTransform = GetComponent<Transform>();

        OnEnable();
    }

    void OnEnable()
    {
        wavepointIndex = 0;
        targetPoint = Waypoints.points[wavepointIndex];
    }

    private void Update()
    {
        Vector3 dir = targetPoint.position - myTransform.position;
        myTransform.Translate(dir.normalized * enemy.CurrentSpeed * Time.deltaTime, Space.World);

        if (Vector3.Distance(myTransform.position, targetPoint.position) <= 0.4f)
        {
            GetNextWaypoint();
        }

        enemy.CurrentSpeed = enemy.MaxSpeed; //возвращаем юниту изначальную скорость, если он был замедлен в предыдущем кадре
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            gameManager.ReduceLivesAmount();
            spawnSystem.PutToThePool(gameObject, enemy.EnemyName);
        }
        else
        {
            wavepointIndex++;
        }
        targetPoint = Waypoints.points[wavepointIndex];
    }
}

