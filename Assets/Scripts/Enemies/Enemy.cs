﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] string enemyName;
    [SerializeField] float maxHealth;
    [SerializeField] float maxSpeed = 10f;
    int worth = 50;//стоимость юнита
    float currentSpeed;
    float currentHealth;
    SpawnSystem spawnSystem;
    GameManager gameManager;

    public float CurrentHealth { get => currentHealth; }
    public string EnemyName { get => enemyName; }
    public float MaxSpeed { get => maxSpeed;}
    public float CurrentSpeed { get => currentSpeed; set => currentSpeed = value; }

    private void Start()
    {
        spawnSystem = FindObjectOfType<SpawnSystem>();
        gameManager = FindObjectOfType<GameManager>();
        OnEnable();
    }

    private void OnEnable()
    {
        currentHealth = maxHealth;
        currentSpeed = maxSpeed;
    }

    internal void Slowdown(float slowdownPercentage)
    {
        currentSpeed = maxSpeed * (1f - slowdownPercentage);
    }

    public void TakeDamage(Tower turret, float amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            turret.EnemiesInRange.Remove(gameObject);
            spawnSystem.PutToThePool(gameObject, enemyName);
            gameManager.EnlargeMoneyAmount(worth);
        }
    }
}

