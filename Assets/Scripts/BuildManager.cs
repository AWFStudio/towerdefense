﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;
    TowerBlueprint towerToBuild;
    UIController wallet;
    GameManager game;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one BuildManager in scene!");
            return;
        }
        instance = this;
        wallet = FindObjectOfType<UIController>();
        game = FindObjectOfType<GameManager>();
    }

    public bool CanBuild { get { return towerToBuild != null; } }
    public bool HasMoney { get { return game.Money >= towerToBuild.towerCost; } }


    public void SelectTowerToBuild(TowerBlueprint turret)
    {
        towerToBuild = turret;
    }

    internal void BuildTowerOn(Node node)
    {
        if (game.Money < towerToBuild.towerCost)
        {
            return;
        }

        game.ReduceMoneyAmount(towerToBuild.towerCost);
        GameObject tower = Instantiate(towerToBuild.towerPref, node.GetBuildPosition(), Quaternion.identity);
        node.Tower = tower;
        node.ShowBuildEffect();
    }
}
